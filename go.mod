module gitlab.com/thorchain/bifrost/ltcd-txscript

go 1.15

require (
	github.com/btcsuite/btclog v0.0.0-20170628155309-84c8d2346e9f
	github.com/davecgh/go-spew v0.0.0-20171005155431-ecdeabc65495
	github.com/ltcsuite/ltcd v0.20.1-beta
	github.com/ltcsuite/ltcutil v1.0.2-beta
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
